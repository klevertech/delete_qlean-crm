export const state = () => ({
  counter: 0,
  token: '',
  cleaner: {},
  dataRow: {}
})

export const mutations = {
  token(state, token) {
    state.token = token
  },
  cleaner(state, cleaner) {
    state.cleaner = cleaner
  },
  dataRow(state, dataRow) {
    state.dataRow = dataRow
  }
}
